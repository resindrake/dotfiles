#!/bin/bash
for FILE in $(find ~resin/.dotfiles/); do
	FILEPART=$(echo $FILE | sed 's/$HOME//')
	if [[ $FILE != */.git* && $FILE != */gen.sh  ]]; then
		echo $FILEPART
		if [[ -f $FILE ]]; then
			:
		elif [[ -d $FILE ]]; then
			:
		fi
	fi
done
