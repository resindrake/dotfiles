# Default Terminal
set -g default-terminal "screen-256color"

# Status Bar
set -g status-style 'bg=colour0 fg=colour7 dim'
set -g status-left ''
set -g status-right '#[fg=colour15,bg=colour239] CPU: #{cpu_fg_color}#{cpu_percentage}#[fg=colour15] RAM: #{ram_fg_color}#{ram_percentage} #[fg=colour233,bg=colour247] %b %d %H:%M '

# Status Bar Windows
setw -g window-status-current-style 'fg=colour2 bg=colour247 bold'
setw -g window-status-current-format ' #I#[fg=colour237]:#[fg=colour255]#W#[fg=colour237]#F '

setw -g window-status-style 'fg=colour9 bg=colour239'
setw -g window-status-format ' #I#[fg=colour249]:#[fg=colour250]#W#[fg=colour249]#F '

# Mouse Mode
set -g mouse on

# set first window to index 1 (not 0) to map more to the keyboard layout
set-option -g renumber-windows on
set -g base-index 1
setw -g pane-base-index 1

# remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# split panes using | and -
bind | split-window -h
bind - split-window -v
unbind '"'
unbind %

# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf

# switch panes using Alt-arrow without prefix
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# Disable all bells
set -g visual-activity off
set -g visual-bell off
set -g visual-silence off
setw -g monitor-activity off
set -g bell-action none

# Custom CPU Display
set -g @cpu_low_fg_color ""

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-cpu'
set -g @plugin 'tmux-plugins/tmux-sensible'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'

## Clipboard integration
# ctrl+c to send to clipboard
bind C-c run "tmux save-buffer - | xclip -i -sel clipboard"
# ctrl+v to paste from clipboard
bind C-v run "tmux set-buffer \"$(xclip -o -sel clipboard)\"; tmux paste-buffer"

# Selection with mouse should copy to clipboard right away, in addition to the default action.
unbind -n -T copy-mode-vi MouseDragEnd1Pane
bind -T copy-mode-vi MouseDragEnd1Pane send -X copy-selection-and-cancel\; run "tmux save-buffer - | xclip -i -sel clipboard > /dev/null"

# Middle click to paste from the clipboard
unbind-key MouseDown2Pane
bind-key -n MouseDown2Pane run "tmux set-buffer \"$(xclip -o -sel clipboard)\"; tmux paste-buffer"

# Drag to re-order windows
bind-key -n MouseDrag1Status swap-window -t=

# Double click on the window list to open a new window
bind-key -n DoubleClick1Status new-window
